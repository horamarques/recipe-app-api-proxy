3 recipe api paroxy

NGINX proxy app for our recipe app API

## Usage

### Enviroment variables

* `LISTEN_PORT`- Porto to listen on (default: `8080`)
* `APP_HOST`- Hostname of the app to forward requests to (default: `app`)
* `APP_PORT`- Port of the app to forward requests to (default: `9000`)